# Auteur : David Lefebvre
# Nom : client.py
# Version : 0.0     24/03/2018 - Creation - David Lefebvre
#           0.1     25/03/2018 - Fonctionnel - David Lebvre
#                   26/03/2018 - Correction - groupe

# ======================================================================
# ======================================================================

from os import system
import socket

from Crypto.PublicKey import RSA
from Crypto.PublicKey import ElGamal

from Crypto.Cipher import DES3
from Crypto.Cipher import DES
from Crypto.Cipher import AES

from Crypto import Random
from Crypto.Random import random
from Crypto.Util.number import GCD

# ======================================================================
# ======================================================================
# Permet de choisir l'algo symetrique ou assymetrique.

def choix_algo(algos) :
    demander = True
    while demander :
        for ind, algo in enumerate(algos) :
            print(ind, "-", algo)
        algoInd = input("Choisissez le numero de l'algorithme voulu : ")
        try :
            algo = algos[int(algoInd)]
            demander = False
        except:
            print("Erreur de saisie.")
    return(algo)


# ======================================================================
# ======================================================================

system('clear')

asymPos = ['RSA', 'ElGamal', 'DiffieHelman']
symPos = ["AES", "DES", "DES3"] # Jeter un oeil a DES3

asym_algo = choix_algo(asymPos)
sym_algo = choix_algo(symPos)
conn_string = "connect_" + asym_algo + "_" + sym_algo

    # Initialisation du socket
srvr = socket.socket()
    # Tentative de connexion
srvr.connect(("127.0.0.1",10443))
    # Envoie de la requete
srvr.send(conn_string.encode())
print("Commande envoyee...\n")
    # Ecoute de la reponse du serveur
rep = srvr.recv(1024)
print("Reponse du serveur :", rep, "\n")

if rep == b'OK':
        # Envoie de la cle publique, verification de l'algo utilise
    if asym_algo == "RSA":
        cleAsym = RSA.generate(1024)
        cleAsymPub = cleAsym.publickey().exportKey('PEM')
        srvr.send(cleAsymPub)
        print("Envoi de la clé publique...")
        print(cleAsymPub.decode('utf-8'), "\n")
    elif asym_algo == "ElGamal":
        cleAsym = ElGamal.generate(512, Random.new().read)
        cleAsymPub = cleAsym.publickey()
        comps = ('p', 'g', 'y')
        out = eval("b'" + ":".join(["{}:".format(getattr(cleAsymPub, comp)) for comp in comps])+"'")
        srvr.send(out)
        print("Envoie de la cle publique...")
        print(out)
    elif asym_algo == "DiffieHelman":
        pass
    # end if
    
        # Recoie la cle symetrique
    cle_sym = srvr.recv(1024)
    cle_sym = cleAsym.decrypt(cle_sym)
    print("Cle {} claire :".format(sym_algo))
    print(cle_sym, "\n")
else :
    print("Connexion non etablie.")
# end if

msg = input("Ecrivez votre message : ")

    # Envoie de donnees chiffrees

    # Envoie requete
iv = Random.new().read(eval(sym_algo).block_size)
chiffrementQ = eval(sym_algo).new(cle_sym, eval(sym_algo).MODE_CFB, iv)
http_req = iv + chiffrementQ.encrypt(msg)
srvr.send(http_req)
print("Message envoye...\n")

    # Reception reponse
http_req = srvr.recv(1024)
iv = http_req[:eval(sym_algo).block_size]
chiffrementR = eval(sym_algo).new(cle_sym, eval(sym_algo).MODE_CFB, iv)
req_clair = chiffrementR.decrypt(http_req[eval(sym_algo).block_size:]).decode('utf-8')
print("Réponse (claire) du serveur : ", req_clair)

    # Ferme la connexion
srvr.close()
