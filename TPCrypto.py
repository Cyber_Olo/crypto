# Auteur : Olivier de Casanove
# Nom : TPCrypto.py
# Version : 0.0     17/02/2018 - creation
#           0.1     03/03/2018 - Diffie Helmann ajoute
# Derniere modification : 05/03/2018 - RSA en chantier

# ======================================================================
# ======================================================================


from os import system

from Crypto.Util.number import GCD

from Crypto import Random
from Crypto.Random import random

from Crypto.PublicKey import RSA
from Crypto.PublicKey import ElGamal

from Crypto.Cipher import DES
from Crypto.Cipher import AES


# ======================================================================
# ======================================================================

# Variables globales

clePubAlice = ''
clePubBob = ''


# ======================================================================
# ======================================================================


def calcMod(nbr, pui, mod) :
    val = (nbr**pui) % mod
    return(val)

# ======================= #
# ===  Diffie Helman  === #
# ======================= #


def echangeCle() :
    """Echange de cles de Diffie Helman."""
        # Creation des joueurs
    parleAlice, parleBob = StringIO(), StringIO()
    condAlice, condBob = Condition(), Condition()
    alice = JoueurDH(parleBob, condBob, parleAlice, condAlice, "Alice")
    bob = JoueurDH(parleAlice, condAlice, parleBob, condBob, "Bob")
        # Gestion de la partie commune
    prem = 17 # getStrongPrime(2048)
    gen = 2 # comment on recupere une racine ?
    alice.accord(prem, gen)
    bob.accord(prem, gen)
    alice.start()
    bob.start()

# ============= #
# ===  RSA  === #
# ============= #

""" Cles RSA """

def partieRSA() :
    gen = RSA.RSAImplementation()
    RSAAlice = gen.generate(2048)
    print(">> RSA private key generated.")
    mes = "Hello world!"
    print("Message : {}".format(mes))
    mes = eval("b'" + mes + "'")
        # Autre trousseau
    pubCle = RSAAlice.publickey()
    mesCh = pubCle.encrypt(mes, 32)
    print("Message chiffre : {}".format(mesCh))
    mes = RSAAlice.decrypt(mesCh)
    print("Message dechiffre : {}".format(mes))
    
# ================== #
# ===  El Gamal  === #
# ================== #

""" Cles El Gamal """

def partieElGamal(mes) :
    print("Message : {}".format(mes))
    cle = ElGamal.generate(512, Random.new().read)
    print("Cle : {}".format(cle))
    clePub = cle.publickey()
    print(clePub)
    mesCh = clePub.encrypt(mes, 32)
    print("Message chiffre : {}".format(mesCh))
    mes = cle.decrypt(mesCh)
    print("Message dechiffre : {}".format(mes))

# ============= #
# ===  DES  === #
# ============= #

""" Communication DES"""

def partieDES() :
    mes = b"Hello world!"
    print("Message : {}".format(mes))
    cle = b"juliette"
    print("Cle : {}".format(cle))
    iv = Random.new().read(DES.block_size)
    chiffrement = DES.new(cle, DES.MODE_CFB, iv)
    enc = iv + chiffrement.encrypt(mes)
    print("Message chiffre : {}".format(enc))
    iv = enc[:8]
    chiffrement2 = DES.new(cle, DES.MODE_CFB, iv)
    dec = chiffrement2.decrypt(enc[8:])
    print("Message dechiffre : {}".format(dec))

# ============= #
# ===  AES  === #
# ============= #

""" Communication AES"""

def partieAES() :
    mes = b"Hello world!"
    print("Message : {}".format(mes))
    cle = b"Sixteen byte key"
    iv = Random.new().read(AES.block_size)
    chiffrement = AES.new(cle, AES.MODE_CFB, iv)
    enc = iv + chiffrement.encrypt(mes)
    print("Message chiffre : {}".format(enc))
    iv = enc[:16]
    chiffrement2 = AES.new(cle, AES.MODE_CFB, iv)
    dec = chiffrement2.decrypt(enc[16:])
    print("Message dechiffre : {}".format(dec))

# ============== #
# ===  DES3  === #
# ============== #

""" Communication DES3"""

# ======================================================================
# ======================================================================

# ============================== #
# ===  Programme Principale  === #
# ============================== #

system('clear')
mes = b'Hello world!'
#print("\n===  Echange de cle par la methode de Diffie Hellman.  ===\n")

print("\n===  Exemple RSA  ===\n")
partieRSA()

print("\n===  Exemple ElGamal  ===\n")
partieElGamal(mes)

print("\n===  Exemple DES  ===\n")
partieDES()

print("\n===  Exemple AES  ===\n")
partieAES()
