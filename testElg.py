from Crypto.PublicKey import ElGamal
from Crypto.Random import get_random_bytes

key = ElGamal.generate(256, get_random_bytes)
pubkey = key.publickey()
print("pubkey :", pubkey)
print(pubkey.keydata)
comps = ('p', 'g', 'y')
out = [getattr(pubkey, comp) for comp in comps]
key2 = ElGamal.construct((getattr(pubkey, 'p'), getattr(pubkey, 'g'), getattr(pubkey, 'y')))
print("key2 :", key2)
msg = "Hello world!"
print(msg)
print("key2 peut chiffrer :", key2.can_encrypt())
ch = key2.encrypt(msg, 32)
print(ch)
print("key peut dechiffrer :", key.can_decrypt())
dch = key.decrypt(msg)
print(dch)
#key2 = ElGamal.construct((out[0], out[1], out[2]))
#print(key2)
