# Auteur : David Lefebvre
# Nom : client.py
# Version : 0.0     24/03/2018 - Creation - David Lefebvre
#           0.1     25/03/2018 - Fonctionnel - David Lebvre
#                   26/03/2018 - Correction - groupe

# ======================================================================
# ======================================================================

from os import system
import socket

from Crypto.PublicKey import RSA
from Crypto.PublicKey import ElGamal

from Crypto.Cipher import DES3
from Crypto.Cipher import DES
from Crypto.Cipher import AES

from Crypto import Random
from Crypto.Random import random
from Crypto.Util.number import GCD


# ======================================================================
# ======================================================================

system('clear')


    # Initialisation du serveur
print("Le serveur a demarre.\n")
s = socket.socket()

    # Ecoute
s.bind(('', 10443))
req_claire = ''
while req_claire != "stop" :
    print("Serveur en attente...\n")
    s.listen(5)         # Jusqu'a 5 connexions
    client, addr = s.accept()
    print("Connexion client :")
    print(client, addr, "\n")

        # Connexion acceptee
    raw_msg = client.recv(1024).decode('utf-8')
    client.send("OK".encode())
    print("Reçu :")
    print(raw_msg, "\n")
    requete = raw_msg.split('_')

        # Si la requete est correcte
    if len(requete) == 3:
        asym_algo = requete[1]
        sym_algo = requete[2]
        raw_msg = client.recv(1024)
        print("Message recu :")
        print(raw_msg.decode(),"\n\n")
            # Lecture du type d'algo asymetrique
        if asym_algo == "RSA" :
            clePub = RSA.importKey(raw_msg)
        elif asym_algo == "ElGamal":
            clePub = raw_msg.decode()
            clePub = clePub.split(':')[1:]
            clePub = ElGamal.construct((int(clePub[0]), int(clePub[1]), int(clePub[2])))
        elif asym_algo == "DiffieHelman":
            pass
            # End if
        print("Cle publique :")
        print(clePub, "\n\n")
        sym_cle = Random.new().read(eval(sym_algo).block_size)
        msg = clePub.encrypt(sym_cle, 32)[0]
        print("Cle symetrique envoyee :")
        print(msg, "\n\n")
        client.send(msg)
    else:
        print("Erreur syntaxe de la requete.")
    # End

        # Dechiffrement du vrai message.
    http_req = client.recv(1024)
    print("Donnees recues...\n")
    iv = http_req[:eval(sym_algo).block_size]
    chiffrementQ = eval(sym_algo).new(sym_cle, eval(sym_algo).MODE_CFB, iv)

    req_claire = chiffrementQ.decrypt(http_req)
    req_claire = req_claire[eval(sym_algo).block_size:].decode("utf-8")
    print("Req claire :")
    print(req_claire, "\n")

    iv = Random.new().read(eval(sym_algo).block_size)

    chiffrementR = eval(sym_algo).new(sym_cle, eval(sym_algo).MODE_CFB, iv)
    msg = iv + chiffrementR.encrypt(b'200 OK at dawn\n')
    client.send(msg)
    print("Envoye : ")
    print(msg, "\n")

        # Fermeture de la connexion
    client.close()
s.close()
